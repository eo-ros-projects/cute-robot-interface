#include <array>
#include <control_msgs/JointTrajectoryControllerState.h>
#include <cmath>
//#include <hardware_interface/joint_command_interface.h>
//#include <hardware_interface/joint_state_interface.h>
//#include <hardware_interface/robot_hw.h>
#include <memory>
#include <ros/callback_queue.h>
#include <ros/ros.h>
#include <ros/spinner.h>
#include <trajectory_msgs/JointTrajectory.h>

#include "ImportURDF.hpp"
#include "MathUtils.hpp"
#include "RobotTree.hpp"

class MyCuteGazeboInterface
{
public:
    MyCuteGazeboInterface(const std::string urdf);
    
    void AddRobotTree(const RobotTree& tree);
    bool AddRobotTreeFromURDF(const std::string urdf_param_name);
    

    void CuteStateCallback(const control_msgs::JointTrajectoryControllerState::ConstPtr& msg);
    void CuteCmdCallback(const ros::Publisher& pub);
    // void DummyDisconnectCallback(const ros::Publisher& pub);
    
    int CreateGazeboSubPub();
    
    void looping_update();
    
private:
    ros::NodeHandle m_nh;
    RobotTree m_robot;
    std::shared_ptr<RobotTree> rt_callback_stpr;    //for callback function in subscribe/advertise options
    ros::Subscriber m_joint_state_sub;
    ros::Publisher m_joint_cmd_pub;
    double m_loop_rate;
};   //class MyCuteGazeboInterface