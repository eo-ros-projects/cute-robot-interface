#include "my_robot_interface_cute/MyCuteGazeboInterface.hpp"


//TODO: change to an actual interface that isn't robot specific; see
//https://github.com/ros-controls/ros_control/wiki/hardware_interface
//TODO: stop using ros_control version? doesn't work in simulation, need a different interface for that
//If I get a Niryo One, maybe work on interface class then; until then, just make my own interface class


/*class TestJointPosController : public controller_interface::Controller<hardware_interface::PositionJointInterface>
{
    boot init(hardware_interface::)
}

class CuteSimInterface : public hardware_interface::RobotHW
{
public:
    //TODO: what should this do/house?
    // - comm interface (ros pub/sub)
    // - robot model object? (RobotTree/alt)
    // - controller set (pos, force, IK, ...)
    // - human interface stuff (display info, keyboard/mouse/controller input), preferably somewhat reusable
    // 
    CuteSimInterface()
    {
        //TODO
    }

    GetMeasuredState()
    {
        //TODO: integrate and expand CuteStateCallback
    }

    SetDesiredPosVel()
    {
        //TODO
    }
}*/



MyCuteGazeboInterface::MyCuteGazeboInterface(const std::string urdf)
{
    if(!AddRobotTreeFromURDF(urdf))
        ROS_FATAL("RobotTree could not be created or added to MyCuteGazeboInterface");
    if(CreateGazeboSubPub() < 0)
        ROS_FATAL("Could not set up subscriber and publisher for MyCuteGazeboInterface");
    m_loop_rate = 25.0;    // 25 Hz matches measure rate
}

void MyCuteGazeboInterface::AddRobotTree(const RobotTree& tree)
{
    //TODO: use smart pointers/move semantics?
    m_robot = tree;
    rt_callback_stpr = std::make_shared<RobotTree>();
    std::cout << "RobotTree object created successfully" << std::endl;
}

bool MyCuteGazeboInterface::AddRobotTreeFromURDF(const std::string urdf_param_name)
{
    KDL::Tree kdl_tree;
    if(ImportModel::ImportParamToKDL(urdf_param_name, kdl_tree))
    {
        //TODO: replace with ROS_INFO or similar
        std::cout << "Robot model converted to KDL tree\n";
    }
    else
    {
        std::cout << "Robot model conversion to KDL tree failed" << std::endl;
        return false;
    }

    Vector6d temp_grav;
    temp_grav << 0.0, 0.0, 0.0, 0.0, 0.0, -9.806;
    AddRobotTree(RobotTree(kdl_tree, temp_grav));
    return true;
}

void MyCuteGazeboInterface::CuteStateCallback(const control_msgs::JointTrajectoryControllerState::ConstPtr& msg)
{
    m_robot.SetMeasuredJointPositions(msg->actual.positions);
    m_robot.SetMeasuredJointVelocities(msg->actual.velocities);
    m_robot.SetMeasuredJointAccelerations(msg->actual.accelerations);
    m_robot.SetMeasuredJointForces(msg->actual.effort);
}

void MyCuteGazeboInterface::CuteCmdCallback(const ros::Publisher& pub)
{
    
    // std::cout << "Should be publishing command (TODO) at time " << curr_time << std::endl;
    trajectory_msgs::JointTrajectory msg;
    msg.joint_names.push_back("joint1");
    msg.joint_names.push_back("joint2");
    msg.joint_names.push_back("joint3");
    msg.joint_names.push_back("joint4");
    msg.joint_names.push_back("joint5");
    msg.joint_names.push_back("joint6");
    msg.joint_names.push_back("joint7");

    std::array<double, 7> joint_cmd_ampl = {0.5, 0.1, 0.2, 0.25, 0.2, 0.75, 1};
    std::array<double, 7> joint_cmd_freq = {1.2, 1, 2, 0.5, 0.2, 1, 3}; //rad/s
    std::array<double, 7> joint_cmd_offset = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

    //Based on: https://wiki.ros.org/pr2_controllers/Tutorials/Moving%20the%20arm%20using%20the%20Joint%20Trajectory%20Action
    msg.points.resize(1);
    msg.points.at(0).positions.resize(7);
    msg.points.at(0).velocities.resize(7);
    //msg.points.accelerations.resize(7);

    for(size_t i = 0; i < 7; ++i)
    {
        //TODO: add vel/accel, or just pos?
        msg.points.at(0).positions.at(i) = joint_cmd_ampl.at(i)*sin(joint_cmd_freq.at(i)*curr_time) + joint_cmd_offset.at(i);
        msg.points.at(0).velocities.at(i) = joint_cmd_freq.at(i)*joint_cmd_ampl.at(i)*cos(joint_cmd_freq.at(i)*curr_time);
    }
    //Needs to be > 0, else gazebo thinks its in the past (floating point error)
    //TODO: adjust this and observe response just to see?
    msg.points.at(0).time_from_start = ros::Duration(0.001);

    //msg.points.push_back(cmd_positions);
    pub.publish(msg);
}

// void MyCuteGazeboInterface::DummyDisconnectCallback(const ros::Publisher& pub)
// {
//     std::cout << "Publisher disconnecting, shouldn't see this" << std::endl;
// }

int MyCuteGazeboInterface::CreateGazeboSubPub()
{
    //need '&' and namespace/class name to form function pointer from non-static member function
    //'this' use for tracked_object: see cute_hardware_interface.cpp
    m_joint_state_sub = m_nh.subscribe("/cute_arm_controller/state", 1, &MyCuteGazeboInterface::CuteStateCallback, this);
    if(!m_joint_state_sub)
    {
        std::cout << "Subcriber creation failed" << std::endl;
        return -1;
    }

    //TODO: not sure why 'this' for tracked_object behaves differently/not working here...
    m_joint_cmd_pub = m_nh.advertise<trajectory_msgs::JointTrajectory>("/cute_arm_controller/command", 1, 
                            &MyCuteGazeboInterface::CuteCmdCallback/*, &MyCuteGazeboInterface::DummyDisconnectCallback, this*/);
    if(!m_joint_cmd_pub)
    {
        std::cout << "Publisher creation failed" << std::endl;
        return -2;
    }

    return 0;
}

void MyCuteGazeboInterface::looping_update()
{
    ros::Rate loop_rate(m_loop_rate);
    while(ros::ok())
    {
        //TODO: move goal joint position path somewhere (<path_planner>::TestWiggle()?)
        //Path_planner needs: read robot measurements, write robot position commands
        double curr_time = ros::Time::now().toSec();
        TestWiggle(curr_time);

        CuteCmdCallback(m_joint_cmd_pub);
        ros::spinOnce();
        loop_rate.sleep();
    }
}