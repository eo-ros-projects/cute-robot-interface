//TODO

#include <array>
#include <control_msgs/JointTrajectoryControllerState.h>
#include <cmath>
#include <ros/callback_queue.h>
#include <ros/ros.h>
#include <ros/spinner.h>
#include <trajectory_msgs/JointTrajectory.h>

#include "ImportURDF.hpp"
#include "MathUtils.hpp"
#include "RobotTree.hpp"
#include <typeinfo>

void CuteStateCallback(const control_msgs::JointTrajectoryControllerState::ConstPtr& msg)
{
    std::cout << "Measured Joint Positions at time " << ros::Time::now() << ": ";
    for(size_t i = 0; i < msg->actual.positions.size(); ++i)    //msgs use std::vector
        std::cout << msg->actual.positions.at(i) << "  ";
    std::cout << "\n";
}

void CuteCmdCallback(const ros::SingleSubscriberPublisher& pub) //Note: HAVE to pass publisher to compile
{
    double curr_time = ros::Time::now().toSec();
    std::cout << "Should be publishing command (TODO) at time " << curr_time << std::endl;
    trajectory_msgs::JointTrajectory msg;
    msg.joint_names.push_back("joint1");
    msg.joint_names.push_back("joint2");
    msg.joint_names.push_back("joint3");
    msg.joint_names.push_back("joint4");
    msg.joint_names.push_back("joint5");
    msg.joint_names.push_back("joint6");
    msg.joint_names.push_back("joint7");

    std::array<double, 7> joint_cmd_ampl = {0.5, 0.1, 0.2, 0.25, 0.2, 0.75, 1};
    std::array<double, 7> joint_cmd_freq = {1.2, 1, 2, 0.5, 0.2, 1, 3}; //rad/s
    std::array<double, 7> joint_cmd_offset = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

    //Based on: https://wiki.ros.org/pr2_controllers/Tutorials/Moving%20the%20arm%20using%20the%20Joint%20Trajectory%20Action
    msg.points.resize(1);
    msg.points.at(0).positions.resize(7);
    msg.points.at(0).velocities.resize(7);
    //msg.points.accelerations.resize(7);

    //Both JointTrajectoryPoint and positions are vectors - need to use push_back()
    // std::cout << typeid(msg).name() << "\n" << typeid(msg.points).name() << "\n" << typeid(msg.points[0].positions).name() << std::endl;
    // std::cout << msg.points.size() << "\n" << msg.points.at(0).positions.size() << std::endl;

    //trajectory_msgs::JointTrajectoryPoint joint_positions;
    //std::vector<double> cmd_positions;
    for(size_t i = 0; i < 7; ++i)
    {
        //TODO: add vel/accel, or just pos?
        msg.points.at(0).positions.at(i) = joint_cmd_ampl.at(i)*sin(joint_cmd_freq.at(i)*curr_time) + joint_cmd_offset.at(i);
        msg.points.at(0).velocities.at(i) = joint_cmd_freq.at(i)*joint_cmd_ampl.at(i)*cos(joint_cmd_freq.at(i)*curr_time);
    }
    //TODO: is goal_time optional? remove it to reduce delay?
    msg.points.at(0).time_from_start = ros::Duration(0.001);

    //msg.points.push_back(cmd_positions);
    pub.publish(msg);
}

void CuteCmdCallbackSync(const ros::Publisher& pub) //version to call manually around ros::spinOnce()
{
    double curr_time = ros::Time::now().toSec();
    std::cout << "Should be publishing command at time " << curr_time << std::endl;
    trajectory_msgs::JointTrajectory msg;
    msg.joint_names.push_back("joint1");
    msg.joint_names.push_back("joint2");
    msg.joint_names.push_back("joint3");
    msg.joint_names.push_back("joint4");
    msg.joint_names.push_back("joint5");
    msg.joint_names.push_back("joint6");
    msg.joint_names.push_back("joint7");

    std::array<double, 7> joint_cmd_ampl = {0.5, 0.1, 0.2, 0.25, 0.2, 0.75, 1};
    std::array<double, 7> joint_cmd_freq = {1.2, 1, 2, 0.5, 0.2, 1, 3}; //rad/s
    std::array<double, 7> joint_cmd_offset = {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0};

    //Based on: https://wiki.ros.org/pr2_controllers/Tutorials/Moving%20the%20arm%20using%20the%20Joint%20Trajectory%20Action
    msg.points.resize(1);
    msg.points.at(0).positions.resize(7);
    msg.points.at(0).velocities.resize(7);
    //msg.points.accelerations.resize(7);

    for(size_t i = 0; i < 7; ++i)
    {
        msg.points.at(0).positions.at(i) = joint_cmd_ampl.at(i)*sin(joint_cmd_freq.at(i)*curr_time) + joint_cmd_offset.at(i);
        msg.points.at(0).velocities.at(i) = joint_cmd_freq.at(i)*joint_cmd_ampl.at(i)*cos(joint_cmd_freq.at(i)*curr_time);
    }
    //Need to be >0, else gazebo/something else says it's in the past (floating point error)
    msg.points.at(0).time_from_start = ros::Duration(0.001);

    pub.publish(msg);
}

void DummyDisconnectCallback(const ros::SingleSubscriberPublisher& pub)
{
    std::cout << "Publisher disconnecting, shouldn't see this" << std::endl;
}

int main(int argc, char *argv[])
{
    //Test RobotTree creation from KDL

    std::string lump_robot_descrip;
    std::string robot_desc_name("/robot_description");
    KDL::Tree kdl_tree;

    ros::init(argc, argv, "my_cute_robot_control_interface");
    ros::NodeHandle nh;

    if(ImportModel::ImportParamToKDL(robot_desc_name, kdl_tree))
    {
        //TODO: replace with ROS_INFO or similar
        std::cout << "Robot model converted to KDL tree\n";
    }
    else
    {
        std::cout << "Robot model conversion to KDL tree failed" << std::endl;
        return -1;
    }

    Vector6d temp_grav;
    temp_grav << 0.0, 0.0, 0.0, 0.0, 0.0, -9.806;
    RobotTree cute_tree(kdl_tree, temp_grav);

    std::cout << "RobotTree object created successfully" << std::endl;


    //Test message connection (TODO: make this YAML configurable? parse file, create container of publishers/subscribers)
    //Note: buffer (really a queue) >1 results in successive calls to callback function in each spin if loop rate < message rate

    ros::CallbackQueue sub_queue;
    ros::SubscribeOptions sub_ops = ros::SubscribeOptions::create<control_msgs::JointTrajectoryControllerState>(
                "/cute_arm_controller/state", 1, CuteStateCallback, ros::VoidPtr(), &sub_queue); //NULL, &sub_queue
    sub_ops.allow_concurrent_callbacks = true;
    ros::Subscriber cute_state_sub = nh.subscribe(sub_ops);
    // ros::Subscriber cute_state_sub = nh.subscribe("/cute_arm_controller/state", 1, CuteStateCallback);
    
    if(!cute_state_sub)
    {
        std::cout << "Subcriber creation failed" << std::endl;
        return -1;
    }

    //TODO: ROS documentation recommends using Timers instead of Rate, but not sure how to match that with cuteStateCallback
    // ros::Rate sub_loop_rate(1);
    // while(ros::ok())
    // {
    //     ros::spinOnce();
    //     sub_loop_rate.sleep();
    // }

    //Successfully gets joint state, now need to 1) publish commands, 2) modify RobotTree state

    //Test different publish/subscribe rates
    //TODO: spin/spinOnce will trigger all publisher/subscriber callbacks; couple options for separate rates
    // 1) Timers, but not sure how to create them with the right info yet + not recommended for hard real-time behaviors
    // 2) Split publisher, subscriber into different nodes (diff code entirely) or different threads
    //(MultiThreadedSpinner/AsyncSpinner) - need to split them into different callback queues somehow
    //Looks like no clear way to set timing on AsyncSpinner, so use it to publish whenever
    //Control subcriber (measure) rate with internal queue + ros::Rate

    ros::CallbackQueue pub_queue;
    ros::AdvertiseOptions pub_ops = ros::AdvertiseOptions::create<trajectory_msgs::JointTrajectory>(
            "/cute_arm_controller/command", 1, CuteCmdCallback, DummyDisconnectCallback, ros::VoidPtr(), NULL); //&pub_queue, NULL
    pub_ops.latch = true;
    ros::Publisher cute_cmd_pub = nh.advertise(pub_ops);
    // ros::Publisher cute_cmd_pub = nh.advertise<trajectory_msgs::JointTrajectory>("/cute_arm_controller/command", 1);
    if(!cute_cmd_pub)
    {
        std::cout << "Publisher creation failed" << std::endl;
        return -1;
    }

    //AsyncSpinner rate can't be controlled (hence async), so can't do two different+defined pub/sub rates in single node?
    //Also, looks like I can't assume "as fast as possible" - it seems to only occur a couple times then stop
    ros::AsyncSpinner async_spinner_sub(1, &sub_queue);
    async_spinner_sub.start();

    // ros::AsyncSpinner async_spinner_pub(1, &pub_queue);
    // async_spinner_pub.start();

    //Definitely looks like publish framework is very asynchronous and competes with subscribers - when subscribed to a 
    //high execution rate topic and trying to publish, low-rate publish can get overwhelmed if in same node.
    //Async publishing seems to fail when paired with "rapid" subscriber, even if subscriber callback called less frequently
    //(doesn't mean the messages happen less, just more are dropped through the buffer)
    //Solutions: publish and subscribe at same rate, or publish at defined rate and subscribe async

    ros::Rate loop_rate(50);
    while (ros::ok())
    {  
        CuteCmdCallbackSync(cute_cmd_pub);

        ros::spinOnce();
        loop_rate.sleep();
    }

    //Maybe create container class for RobotTree (inheret from 'Robot' TODO) and message interface, translate messages and
    //update RobotTree state
    //For gazebo sim: minimum messages are cute_arm_controller/command and cute_arm_controller/state
    
    return 0;
}